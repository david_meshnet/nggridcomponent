var gulp = require('gulp');
var concat = require('gulp-concat');
var templateCache = require('gulp-angular-templatecache');
var connect = require('gulp-connect');

gulp.task('template', function () {
    return gulp.src('src/*.template.html')
        .pipe(templateCache('tmp.js', {
            module: 'ngGridComponent'
        }))
        .pipe(gulp.dest('tmp/'));
});

gulp.task('build', ['template'], function () {
    return gulp.src(['src/grid.module.js', 'src/grid.component.js', 'tmp/tmp.js'])
        .pipe(concat('ngGridComponent.js'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('serve', function () {
    connect.server({
        root: ['src', 'bower_components'],
        livereload: true,
        port: 3000
    });

    gulp.watch(['./src/*.*'], ['html']);
});

gulp.task('html', function () {
    gulp.src('./src/*.*')
        .pipe(connect.reload());
});
