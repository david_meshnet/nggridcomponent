(function () {
    'use strict';

    angular
        .module('ngGridComponent', []);
})();

(function () {
    'use strict';

    angular
        .module('ngGridComponent')
        .component('ngGrid', {
            bindings: {
                gridOptions: "<",
                filter: "=",
                onUpdate: "&",
                onDelete: "&",
                onCreate: "&"

            },
            templateUrl: 'grid.template.html',
            controller: Controller,
            controllerAs: 'vm'




        });

    Controller.$inject = ['$templateCache', '$scope', '$sce'];

    /* @ngInject */
    function Controller($templateCache, $scope, $sce) {
        console.log(this.gridOptions)
        var vm = this;

        console.log(vm.onUpdate.toString())
        if (!this.filter) {
            vm.filterBinding = {}
        } else {
            vm.filterBinding = this.filter
        };
        
        vm.showFilter = false;


        vm.sortBy = function (column) {

            var temp = vm.gridOptions.columns[column].sorted;
            angular.forEach(vm.gridOptions.columns, function (item) {
                item.sorted = 0
            })
            if (temp == null || temp == 0) temp = -1
            vm.gridOptions.columns[column].sorted = temp * -1
            if(vm.gridOptions.data) vm.gridOptions.data = vm.gridOptions.data.sort(function (a, b) {

                // will need to improve this for multiple column sort    
                return a[column] < b[column] ? vm.gridOptions.columns[column].sorted * -1 : vm.gridOptions.columns[column].sorted;
            })




        }

        // set all null sorting to 0 and sort by any coloumn that is set to true
        angular.forEach(vm.gridOptions.columns, function (item, id) {
            if (!item.sorted) item.sorted = 0
            else {
                item.sorted = 0
                vm.sortBy(id)
            }
        })

        vm.colCount = function(){
            var count = 0
            angular.forEach(vm.gridOptions.columns,function(item){if( item.hidden!=true) count++})
            return count;
        }

        vm.showMenu = function () {
            vm.menuVisible = vm.menuVisible ? false : true
        }

        vm.filterMenu = {}
        
        
        
//        vm.showFilter = function (id) {
//            if (vm.filterRow && vm.filterRow.show) {
//                vm.filterRow.show = false;
//            } else {
//                vm.filterRow = {
//                    show: true
//                };
//            }
//
//
//        }



        angular.forEach(vm.gridOptions.columns, function (item, id) {
            console.log(item, id)
            $templateCache.put("ngGrid-" + id, item.template)
            $templateCache.put("ngGrid-edit-" + id, item.editTemplate)
        })


        vm.getTemplate = function (id,edit) {
            if (!edit) return 'ngGrid-' + id;
            if (edit) return 'ngGrid-edit-' + id
        }
        
        if(vm.gridOptions.rows.editTemplate){
            $templateCache.put("ngGrid-rowcell-default-edit",vm.gridOptions.rows.editTemplate);
        }
        
        if(vm.gridOptions.rows.template){
            $templateCache.put("ngGrid-rowcell-default",vm.gridOptions.rows.template);
        }
        
        vm.getDefaultTemplate = function(edit){
            if (!edit) return 'ngGrid-rowcell-default'
            if (edit) return 'ngGrid-rowcell-default-edit'
        }



    }




})();

angular.module('ngGridComponent').run(['$templateCache', function($templateCache) {$templateCache.put('grid.template.html','<div class="data-grid">\n    <div class="grid-header">\n        <div class="grid-item grid-item-icon" ng-click="vm.showMenu()">\n            <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;\n        </div>\n\n        <div class="grid-item" ng-repeat="(id,item) in vm.gridOptions.columns" ng-hide="vm.gridOptions.columns[id].hidden" ng-class="{ \'sorted\': vm.gridOptions.columns[id].sorted!=0, \'grid-item-small\': vm.gridOptions.columns[id].size == \'small\', \'grid-item-medium\': vm.gridOptions.columns[id].size == \'medium\', \'grid-item-large\': vm.gridOptions.columns[id].size == \'large\'}">\n            <div class="grid-item-wrapper">\n                <div class="text" ng-click="vm.sortBy(id)">{{item.name}}</div>\n                <div class="icon" ng-click="vm.sortBy(id)"><i ng-if="item.sorted!=0" class="fa" ng-class="item.sorted==1?\'fa-caret-up\':\'fa-caret-down\'"></i>\n                </div>\n                <div class="icon" ng-click="vm.showFilter=!vm.showFilter"><i ng-class="vm.filterBinding[id].length?\'filtered\':\'\'" title="{{vm.filterBinding[id]}}" class="fa fa-filter fa-pull-right"></i></div>\n            </div>\n        </div>\n    </div>\n    <div class="grid-menu" ng-show="vm.menuVisible" ng-mouseleave="vm.showMenu()">\n        <div>Show Columns</div>\n        <div class="grid-menu-wrapper" ng-repeat="col in vm.gridOptions.columns">\n            <input type="checkbox" value=undefined ng-true-value=undefined ng-false-value="true" ng-model="col.hidden">\n            <label>{{col.name}}</label>\n\n        </div>\n    </div>\n    <div class="grid-row grid-filter" ng-show="vm.showFilter">\n        <div class="grid-filter-item grid-item-icon">\n            <button ng-click="vm.filterBinding={}; vm.showFilter=false"><i class="fa fa-times fa-1x"></i></button>\n        </div>\n        <div class="grid-filter-item" ng-repeat="(id,item) in vm.gridOptions.columns" ng-hide="vm.gridOptions.columns[id].hidden" ng-class="{ \'sorted\': vm.gridOptions.columns[id].sorted!=0, \'grid-item-small\': vm.gridOptions.columns[id].size == \'small\', \'grid-item-medium\': vm.gridOptions.columns[id].size == \'medium\', \'grid-item-large\': vm.gridOptions.columns[id].size == \'large\'}">\n            <div>\n                <input ng-focus="vm.filterMenu[id].show" type="text" ng-model="vm.filterBinding[id]">\n            <button ng-click="vm.filterBinding[id]=\'\'" ng-show="vm.filterBinding[id].length"><i class="fa fa-times fa-1x"></i></button>\n                </div>\n        </div>\n    </div>\n    <div class="grid-content">\n        <div class="grid-content-loading" ng-if="!vm.gridOptions.data">\n            <div class="loader">\n                <svg version="1.1" id="L2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n                    <circle fill="none" stroke="#ccc" stroke-width="4" stroke-miterlimit="10" cx="50" cy="50" r="48"></circle>\n                    <line fill="none" stroke-linecap="round" stroke="#ccc" stroke-width="4" stroke-miterlimit="10" x1="50" y1="50" x2="85" y2="50.5" transform="rotate(318 50 50)">\n                        <animateTransform attributeName="transform" dur="1s" type="rotate" from="0 50 50" to="360 50 50" repeatCount="indefinite"></animateTransform>\n                    </line>\n                    <line fill="none" stroke-linecap="round" stroke="#ccc" stroke-width="4" stroke-miterlimit="10" x1="50" y1="50" x2="49.5" y2="74" transform="rotate(90.4 50 50)">\n                        <animateTransform attributeName="transform" dur="10s" type="rotate" from="0 50 50" to="360 50 50" repeatCount="indefinite"></animateTransform>\n                    </line>\n                </svg>\n            </div>\n        </div>\n        <div ng-show="vm.gridOptions.data">\n            <div class="grid-row" ng-class="{\'highlight\': row.selected}" ng-repeat-start="(rowid,row) in vm.gridOptions.data | filter: vm.filterBinding" ng-class-odd="{\'grid-row-odd\': true}">\n                <div class="grid-item grid-item-icon"><i class="fa" ng-class="row.expand?\'fa-caret-down\':\'fa-caret-right\'" ng-click="row.expand=!row.expand" ng-if="vm.gridOptions.detailTemplate"></i></div>\n                \n                \n                <div  class="grid-item" ng-repeat="(colid,column) in vm.gridOptions.columns" ng-click="row.selected=vm.gridOptions.rows.selectable?!row.selected:false" ng-hide="column.hidden" ng-class="{ \'sorted\': vm.gridOptions.columns[colid].sorted!=0, \'grid-item-small\': vm.gridOptions.columns[colid].size == \'small\', \'grid-item-medium\': vm.gridOptions.columns[colid].size == \'medium\', \'grid-item-large\': vm.gridOptions.columns[colid].size == \'large\'}">\n                \n                    <!--  display without template  -->\n                    <div ng-if="!column.template  && (!(column.edit || row.edit) || !column.editable)">{{row[colid]}}</div>\n                    <!-- display with template -->\n                    <div ng-if="column.template && (!(column.edit || row.edit) || !column.editable)">\n                        <ng-include src=" vm.getTemplate(colid) "></ng-include>\n                    </div>\n                \n                    <!-- edit without template -->\n                    <div ng-if="!column.editTemplate && !vm.gridOptions.rows.editTemplate  && (column.edit || row.edit)  && column.editable"><input ng-model="row[colid]"></div>\n                    \n                    <!-- edit with rows.editTemplate default template -->\n                    <div ng-if="!column.editTemplate && vm.gridOptions.rows.editTemplate && (column.edit || row.edit)  && column.editable"><ng-include src=" vm.getDefaultTemplate(true) "></ng-include></div>\n                \n                    <!-- edit with cell template -->\n                    <div ng-if="column.editTemplate && (column.edit || row.edit) && column.editable">\n                        <ng-include src=" vm.getTemplate(colid,true) "></ng-include>\n                    </div>\n                </div>\n                \n                \n            </div>\n            <div ng-repeat-end="" class="details-container" ng-if="row.expand">\n                <ng-include src="vm.gridOptions.detailTemplate" ng-if="vm.gridOptions.detailTemplate"></ng-include>\n            </div>\n        </div>\n    </div>\n</div>\n');}]);