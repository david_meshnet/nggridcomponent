# ngGrid-component #

Reusable Angular Grid Component for projects.

### What is this repository for? ###

* development of a robust angular data table/grid
* sorting, filtering, expandable detail rows, templating

### How do I get set up? ###

* Ideally this should be used through bower

### Current Status

Grid layout, css, editing and lifecycle callback functionality.


### Usage:

	<ng-grid 
		grid-options="[grid options]" 
		filter="[filter object]" 
		onUpdate="[function]" 
		onDelete="[function]" 
		onCreate="[function] "
	>
	</ng-grid>

grid-options should be in the format of:

	gridOptions = {
                rows: {
                    selectable: true,
                    editTemplate: "<input type='text' ng-model='row.tempRow[colid]'>"
                },
                columns: {
                    name: {
	                    name: 'Name',
	                    size: 'small',
	                    editable: true,
	                    editTemplate: "<input style='width: 9vw;' type='text' placeholder='Enter a name' ng-model='row.tempRow.name'>"
	                },
	                address: {
	                    name: 'Address',
	                    sorted: true,
	                    size: 'medium',
	                    editable: true
	                },
	                phone: {
	                    name: "Phone Number",
	                    template: "<button ng-click='column.dial(row)'> {{row.phone}} </button>",
	                    dial: function(row) {
	                        alert("Dialing... " + row.phone)
	                    },
	                    size: 'small',
	                    editable: true
	                },
	                ]
                },
                data: [{
	                "name": "Daniel Montoya",
	                "address": "Ap #111-9070 Id Rd.",
	                "phone": "1-824-475-7971",
	                
	            }, {
	                "name": "Salvador Rollins",
	                "address": "546-5704 Sollicitudin Rd.",
	                "phone": "1-168-554-3059",
	                
	            }]            		
            }

The following viewmodel variables are avaialble in the templates:

rowid, row: the current row id and data

colid, column: the current column id and column paramaters

vm: the viewmodel for access to onUpdate, onDelete, and onCreate mostly
 
The filter attribute is a two way binding holding the current filter object paramaters for the grid.  This can be used to control of link the grid filtering with external controller.