(function () {
    'use strict';

    angular
        .module('ngGridComponent')
        .component('ngGrid', {
            bindings: {
                gridOptions: "<",
                filter: "=",
                onUpdate: "&",
                onDelete: "&",
                onCreate: "&"

            },
            templateUrl: 'grid.template.html',
            controller: Controller,
            controllerAs: 'vm'




        });

    Controller.$inject = ['$templateCache', '$scope', '$sce'];

    /* @ngInject */
    function Controller($templateCache, $scope, $sce) {
        console.log(this.gridOptions)
        var vm = this;

        
        if (!this.filter) {
            vm.filterBinding = {}
        } else {
            vm.filterBinding = this.filter
        };
        
        vm.showFilter = false;


        vm.sortBy = function (column) {

            var temp = vm.gridOptions.columns[column].sorted;
            angular.forEach(vm.gridOptions.columns, function (item) {
                item.sorted = 0
            })
            if (temp == null || temp == 0) temp = -1
            vm.gridOptions.columns[column].sorted = temp * -1
            if(vm.gridOptions.data) vm.gridOptions.data = vm.gridOptions.data.sort(function (a, b) {

                // will need to improve this for multiple column sort    
                return a[column] < b[column] ? vm.gridOptions.columns[column].sorted * -1 : vm.gridOptions.columns[column].sorted;
            })




        }

        // set all null sorting to 0 and sort by any coloumn that is set to true
        angular.forEach(vm.gridOptions.columns, function (item, id) {
            if (!item.sorted) item.sorted = 0
            else {
                item.sorted = 0
                vm.sortBy(id)
            }
        })

        vm.colCount = function(){
            var count = 0
            angular.forEach(vm.gridOptions.columns,function(item){if( item.hidden!=true) count++})
            return count;
        }

        vm.showMenu = function () {
            vm.menuVisible = vm.menuVisible ? false : true
        }

        vm.filterMenu = {}
        
        
        
//        vm.showFilter = function (id) {
//            if (vm.filterRow && vm.filterRow.show) {
//                vm.filterRow.show = false;
//            } else {
//                vm.filterRow = {
//                    show: true
//                };
//            }
//
//
//        }



        angular.forEach(vm.gridOptions.columns, function (item, id) {
            console.log(item, id)
            $templateCache.put("ngGrid-" + id, item.template)
            $templateCache.put("ngGrid-edit-" + id, item.editTemplate)
        })


        vm.getTemplate = function (id,edit) {
            if (!edit) return 'ngGrid-' + id;
            if (edit) return 'ngGrid-edit-' + id
        }
        
        if(vm.gridOptions.rows.editTemplate){
            $templateCache.put("ngGrid-rowcell-default-edit",vm.gridOptions.rows.editTemplate);
        }
        
        if(vm.gridOptions.rows.template){
            $templateCache.put("ngGrid-rowcell-default",vm.gridOptions.rows.template);
        }
        
        vm.getDefaultTemplate = function(edit){
            if (!edit) return 'ngGrid-rowcell-default'
            if (edit) return 'ngGrid-rowcell-default-edit'
        }



    }




})();
